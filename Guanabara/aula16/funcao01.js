//Uma função com parâmetro e dois retornos,caso o resto o resto 0 retorna par senão retorna impar.
function parimpar(n){
    if(n%2 == 0){
        return 'par!'
    }else{
        return 'impar!'
    }

}
/*
aqui fazemos a chamada pra função, passando como parâmetro o número 4 e 
guardando o retorno da função dentro da variável res.
*/
console.log(parimpar(200))