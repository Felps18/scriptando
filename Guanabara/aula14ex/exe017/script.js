function tabuada(){
    let numero = window.document.getElementById('txtn')
    let tab = document.getElementById('seltab')
    if(numero.value.length == 0){
        window.alert('Por favor, digite um número!')
    }else{
        let num = Number(numero.value)
        let c = 1
        tab.innerHTML=''
        while(c<=10){
            let item = document.createElement('option')
            item.text =`${num} x ${c} = ${num*c}`
            item.value=`tab${c}`
            tab.appendChild(item)
            c++
        }
    }
    
    /*
    for(var c=1; c <= 10; c++){
        
        var total = c*num
        res.innerHTML+=` ${c} x ${num} = ${total} `
        res.innerHTML+='\n'
    }
    */
}