
function contar(){
    var ini = window.document.getElementById('txtini')
    var fim = window.document.getElementById('txtfim')
    var passo = window.document.getElementById('txtpas')
    var res = window.document.querySelector('div#res')

    if(ini.value.length == 0 || fim.value.length == 0 || passo.value.length == 0){
        res.innerHTML='Impossivel contar!'
      //  window.alert('ERROR, faltam dados!')    
    }else{
        res.innerHTML = 'Contando:  <br>'
        let i = Number(ini.value)
        let f = Number(fim.value)
        let p = Number(passo.value)
        if(p <= 0){
            window.alert('Passo inválido! Considerando PASSO 1')
            p = 1
        }
        if( i < f ){
            //Contagem crescente
            for(let c = i ;c <= f; c += p){
                res.innerHTML += `${c}  \u{1F449}`
            }
        }else{
            //Contagem regressiva
            for(let c = i; c >= f;c -=p)
            res.innerHTML += `${c} \u{1}`
        }
        res.innerHTML+= `\u{1F3C1}`
    }
}