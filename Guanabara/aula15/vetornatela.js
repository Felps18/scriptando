let valores = [8, 1, 7, 4, 2, 9]

//Maneira simplificada de mostrar os elementos de um vetor, sem escrever muito código
/*
for(let pos=0; pos< valores.length; pos++){
    console.log(`A posição ${pos} tem o valor ${valores[pos]}`)
}
*/

//Ainda podemos fazer de um jeito mais fácil
for(let pos in valores){
    console.log(`A posição ${pos} tem o valor ${valores[pos]}`)
}