let num = [5, 8, 2, 9, 3]
 
//Adicionando mais um elemento no vetor, com o conteúdo valor 1, irá ficar [5, 8, 2, 9, 3, 1]
num.push(1)

//Organiza os elementos do vetor por ordem crescente, irá ficar [1, 2, 3, 5, 8, 9]
num.sort()

//mostra todo os valores do vetor
console.log(`Nosso vetor é o ${num}`)

//mostra o tamanho do vetor,quantas posições ele tem
console.log(`O vetor tem ${num.length} posições`)

//mostra o primeiro elemento do vetor, caso queira o segundo elemento basta colocar 1
console.log(`O primeiro valor do vetor é ${num[0]}`)

//Buscando a posição d eum determinado valor dentro do vetor
let pos = num.indexOf(8)
if(pos == -1){//ele retorna -1 caso não encontre o valor
    console.log('O valor não foi encontrado!')
}else{
    console.log(`O valor 8 está na posição ${pos}`)
}