*** Aula 4 ***
1-window.alert: mostrar uma mensagem na tela;
ex.: window.alert('Minha primeira mensagem!')

2-window.confirm: mostar uma mensagem na tela com botões de "sim" ou "não",para confirmar algo;
ex.:window.confirm('Está gostando de JS?')

3-window.prompt: mostrar na tela um prompt para preencher algo;
ex.:window.prompt('Qual é seu nome?')

Observação:pode ter ou não ponto e virgula no final de cada comando, ele ira rodar sem nenhum problema.

*** Aula 5 ***
Armazenamento,Tratamento e Operações de Dados

1-sinal de igual em JavaScript temos que interpreta-lo como "recebe" algo; 

2-colocar espaços delimitados dentro da memória(criar variáveis);

3-Em javaScript variável é escrita "var",podendo receber valores(int,float,string,char,boolean e etc...);
ex.v1="Curso em Vídeo"

4-Identificadores:
 4.1-Podem começar com letra,$ ou _;
 4.2-Não podem começar com números;
 4.3-É possível usar acentos e símbolos.Ex: var título = 'filme';
 4.4-Não podem conter espaços.Ex: var car ro = 'Honda';
 4.5-Não podem ser palavras reservadas.Ex: var alert = 'Felipe';

5-Dicas para criar nome de variáveis:
 5.1- Maiúsculas e minúsculas fazem diferença;
 5.2-Tente escolher nomes coeerentes para as variáveis;
 5.3-Evite se tornar um 'programador alfabeto' ou um 'programador contador'

6-Então, variáveis são usadas para guardar dados;

7-Em JavaScript, os tipos primitivos podem ser:
 7.1-number:infinity,Nan;
 7.2-string;
 7.3-boolean;
 7.4-null;
 7.5-undefined;
 7.6-object:Array;
 7.7-function;

8-Pra saber o tipo primitivo de uma variável usamos o "typeof"(tipo de)
Ex.:typeof 6
saída:'number'


Observação: comentários pode ser feito da seguinte forma dentro do Script->
"//"- uma única linha
"/* */"-um bloco

*** Aula 6 ***
Manipulação de Dados

1-Guardando o resultado do prompt que aparecer na janela do google dentro 
de uma variável;
Ex.: var nome = window.prompt('Qual é seu nome?')

2-Para concatenar variaveis a string bastar usar o '+';
Ex.:var nome = window.prompt('Qual é seu nome?')//Vai perguntar o nome...
    window.alert('É um grande prazer em te conhecer,'+ nome +'!')

3- O comando "window.prompt" retorna uma string mesmo digitando um número;

4- Para fazer a adição basta converter a string para número.Basta fazer:
 Number.parseInt(n);
 Number.parseFloat(n);
 
4.1-Nas versões mais novas do javaScript pode ser usar também somente o comando:
Number(n), ele converte para int ou float;

5-Para converter número para String:
String(n);
n.toString();

6-No JavaScript tanto faz usar aspas simples ou aspas duplas para uma string;

7-Para concatenar mais facilmente bastar usar "${}"(Place holder) template string;
Ex.:'o aluno ${nome} com ${idade} anos tirou nota ${nota}'
Irá concatenar as variaveis dentro das aspas sem precisar usar o "+"; 

8-Para medir uma string bastar usar o ".length";
 8.1-Para deixar todas as letras em maiusculo basta usar ".toUpperCase()";
 8.2-Para deixar todas as letras em minúsculo basta usar ".toLowerCase()";

9-Para colocar duas casas após a vírgula basta usar o comando ".toFixed(numero de casas decimais)";
 9.1-Para trocar o ponto para vírgula bastar usar o comando 
".replace('.' , ',')"

10-Para mostrar um salário em reais bastar usar o seguinte comando:
".toLocaleString('pt-BR',{style:'currency',currency:'BRL'})"
Obs:para aparecer em dólar bastar trocar o "BRL" para "USD"

11-Para mostrar uma informação na tela podemos usar o comando:
"document.write"
"documento.writeln"-Para pular linha

Observação: Para comentar no HTML basta usar o "<!-- -->";
Para comentar em CSS basta usar o "/* */" ;
Em JavaScript:
(number + number )para adição;
(string + string) para concatenação;

*** Aula 7 ***
Operadores do JS

1-OPERADORES:
1.1-aritméticos( +, -, *, /, %, **='potência');
 1.2-atribuição;
 1.3-relacionais;
 1.4-lógicos;
 1.5-ternário.

*** Aula 8 ***
Parte 2 de Operadores

1-Nessa aula será abordada os operadores:
 1.1-relacionais;
  1.1.1- ">" Maior;
  1.1.2- "<" Menor;
  1.1.3- ">=" Maior ou igual;
  1.1.4- "<=" Menor ou igual;
  1.1.5- "==" igual;
  1.1.6- "!=" diferente.
 1.2-relacionais de identidade;
  1.2.1-"===" de mesmo tipo primitivo e mesma grandeza.Ex.:'5'==='5' = true; 

 1.3-lógicos;
  1.3.1- "!" negação;
  1.3.2- "&&" conjunção;
  1.3.3- "||" disjunção;

 1.4-ternário.
 1.4.1-teste "?" true ":" false;
 Ex.:média > 7 ? 'APROVADO':'REPROVADO';


*** Aula 9 ***
Introdução ao DOM

1-Baixar e instalar a extensao Watch in chrome no visual studio e 
no google chrome;

2-Baixar e instalar a extensao node Exec no visual studio;

3-DOM:DOCUMENT OBJECT MODEL

4-Árvore DOM, tudo de javaScript está dentro de window;
4.1-Dentro de Window tem: location, document, history;
4.2-Dentro de document tem o HTML e dentro do HTML tem o head e o body;
 4.2.1-Dentro de head tem o meta e o title;
 4.2.2-dentro de body tem o h1, p, p e uma div;

5-Acessando elementos:
 
 5.1-Por Marca;
  5.1.1-getElementsByTagName(), você pode selecionar mais de um objeto ou tag;
  Ex.: var p1 = window.document.getElementsByTagName('p')[0],primeiro paragrafo que aparecer;
  O Comando innerText irá trazer o texto dentro de alguma tag,
  por exemplo "p1.innerText";	
 
 5.2-Por ID;
  5.2.1-getElementsById();
 
 5.3-Por Nome;
  5.3.1-getElementsByName();

 5.4-Por Classe;
  
 5.5-Por Seletor:
  5.5.1-querySelector();
	1-Quando eu quero uma classe colocar o ".",Ex: var d = window.document.querySelector('div.msg');
	2-Quando for um id colocar um "#".Ex: var d = window.document.querySelector('div#msg');
  5.5.2-querySelectorAll();


Observação: pelo Window da para buscar algumas configurações da página HTML,
como por exemplo seu Charset.Ex:window.document.charset;
QuerySelector é um metodo mais novodo ecma Script, pode ser que alguns navegadores nao suportem;

*** Aula 10 ***
Eventos DOM

1-Evento é tudo aquilo que possa acontecer com uma div ou qualquer elemento,
por exemplo eventos de mouse:
1.1-mouseeenter : mover o mouse dentro de uma div, o mouse entrou na div; 
1.2-mousemove : continuar movendo o mouse dentro da div;
1.3-mousedown : clicar e segurar o mouse;
1.4-mouseup : soltar o botao do mouse;
1.5-click : apertar e soltar rapidamente;
1.6-mouseout:mover o mouse para fora da div;

2-Funções ou uma funcionalidade, conjunto de códigos que vão ser executadas só quando o evento ocorrer.
Um bloco de execução só vai ser chamado quando o evento ocorrer e para isso
precisa colocar o bloco entre chaves, nomear ele com a palavra "function" e depois
colocar um nome para essa função, geralmente recebe o nome de uma ação.Pode também
receber parâmetros dentro do parenteses. Por exemplo:
function ação(param){
 //bloco
 } ;

3-Para não poluir muito seu HTML com as chamadas para as ações pode se usar o comando
addEventListener dentro do javaScript, ele irá ficar prestando atenção.Ex.:
a.addEventListener('click',clicar), vai chamar a função quando clicar em cima do elemento;

4-Detecção de erros, dica para descobrir o erro em javaScript :
Ir na página, clicar com o botão direito, ir em "inspecionar", e procurar o Console,
irá mostrar aonde está o erro, por exemplo "linha 26", então sabemos que o erro pode 
estar na linha ou pra cima dela;


Observação: site para ver a lista dos eventos DOM "https://developer.mozilla.org/pt-BR/docs/Web/Events"

*** Aula 11 ***
Módulo D, Condições em JS

*Condições simples(somente um if);
*Condições compostas(if/else);
*Condições aninhadas;
*Condições múltiplas;


1-Sequências, códigos sequenciais tem que seguir o fluxo, porém tem casos em que não
queremos executar um determinado comando,desse modo utilizamos as condições.Criamos uma bifurcação
para seguir um fluxo com desvio, um desvio condicional.Por exemplo:
if(condição==true){//se a condição for verdadeira
  //executa o comando aqui declarado
}else{//se a condição for falsa
  //executa o comando aqui declarado
}

2-Para mostrar o log no console basta usar o comando "console.log()", e aperta a tecla F8;

ATENÇÃO EXERCICÍO: criar em forma de site(html) o exercicío 009.js 

*** Aula 12 ***
 
Condições aninhadas

1-Pegar uma condição composta, e ir icrementando outra condição dentro. Ex:
if(cond){

}else{
   if(cond2){
    
     }else{
     
     }
}

2-Pegar a hora atual.Ex:
var agora= new Date()
var horas = agora.getHours()

3-Condição Múltipla,nos a utilizamos em condições muito especificas,
só funciona com números inteiros e caracteres.Ex:
 switch(expressão){
 case valor1:
  //executa os comandos nesse bloco
  break 
 case valor2:
  //executa os comandos nesse bloco
  break 
 case valor3:
  //executa os comandos nesse bloco
  break 
 case valor4:
  //executa os comandos nesse bloco
  break
 default: 

}
observação:O comnando "break" é obrigatório para cada case aberto;

4-Pegar o dia da semana.Ex:
var agora = new Date()
var diaSem = agora.getDay()

*** MODULO D ***
EXERCICÍOS
1-Pexels portugues, um banco de imagens gratuitas para se utilizar no seu site;
https://www.pexels.com/pt-br/procurar/portugu%C3%AAs/

2- Para carregar imagem no javaScript basta colocar ".src";
ex:img.src = 'fotoTarde.png'

3-Chamando uma função javaScript dentro do body HTML:
<body onload="carregar()">

4-Para centralizar um texto usando javaScript:
res.style.textAlign = 'center';

5-Para criar uma imagem em JavaScript:
var img = document.createElement('img')
img.setAttribute('id','foto')

6-adicionando um elemento para aparecer na tela:
res.appendChild(img)

*** MODULO E ***
Estruturas de repetições

*Repetições com teste no início(while)
*Repetições com teste no final(do/while)
*Repetições com controle(for)
*Exercícios Propostos

*** AULA 13 ***
1-Laço de repetição while, vai ser executado enquando a condição for verdadeira:
while(condição){//"enquando essa condição for verdadeira"
//executa o comando aqui
i++//incrementa na variavel
}

2-Laço de repetição Do/While.Primeiro executa o bloco depois ele testa:
do{
//executa o comando aqui
}while(condição)//"enquando essa condição for verdadeira"


*** AULA 14 ***
1-Laço de repetição com variavel de controle "for".Primeiro um inicio,em seguida um teste lógico e
por ultimo um incremento:
for(var inicio;teste;incr){
//executa o bloco de comando aqui
//podendo colocar qualquer estrutura, de condição ou de repetição
}

Observação: Para debugar seu código no visual studio basta clicar em F5 ou ir em "Debug" e ir em
"Start Debugging", mas antes colocar um "breakPoint" na linha desejada,ao lado do número da linha e apertar uma vez para marcar
com isso poderá ver as variavéis criadas no decorrer das linhas e o valor das mesmas.Para ir linha 
a linha basta clicar "F10" quando o rpograma estiver sendo debugado;

*** MODULO E ***
EXERCICÍOS
 
1- "let" e "var" são praticamente a mesma coisa, porém há diferenças no escopo, pois quando o bloco
de comando acaba o let morre;

2-Lembrando que para pegar um valor do tipo number no javaScript precisamos converter, pois 
ele vem em formato string, então temos que fazer a seguinte conversão:
let i = Number(ini.value)
Pegando o valor de ini e colocando na variavel o valor do tipo Number;

3- Para pegar o codigo de um emoji para colocar no seu HTML, acessar o site:
https://unicode.org/emoji/charts/full-emoji-list.html;

4-Em seguida para implementar no seu JavaScript, colocar o codigo do emoji dentro da crase.
Da seguinte forma:
código do emoji: "U+1F603"
res.innerHTML += `${c}  \u{1F603}`

O codigo do emoji só funciona entre crase;

5-Para criar selects em HTML:
 <select name="tabuada" id="seltab"></select>

criar com as opcões:
<select name="tabuada" id="seltab">
  <option>valor 1</option>
</select>

6-Criar as opcões do select pelo javaScript:
 let item = document.createElement('option')

7-Para selecionar um elemento na tela pelo javaScript:
item.value=`tab${c}`


*** MODULO F ***
AVANÇANDO NOS ESTUDOS

*Variáveis Compostas(array)
*Uso de funções e eventos
*Passagem de Parâmetros

1-Variáveis simples só conseguem aramazenar um valor por vez;

2-Variáveis compostas devem ser capazes de armazenar vários valores 
em uma mesma estrutura;

3-Array vetor, compostos de elementos, no qual o primeiro indice Chave é [0],
e dentro de elemento guarda-se os valores e o espaço ocupado na 
memória.

4-Exemplo de vetor em JavaScript:
let num =[ 5 , 8 , 4 ]

Vetor com três elementos, do indice chave 0 até 2;

5-Para adicionar mais um elemento no vetor, deixando seu tamanho maior basta fazer por
exemplo.:
let num =[ 5 , 8 , 4 ]
num[3] = 6 

ele vai adicionar mais um elemento na posição 3, tendo um conteúdo com valor 6.Portanto agora
temos um array vetor com 4 elementos, com 4 indices de [0] até [3];

OU ainda podemos fazer de uma maneira mais simplificada:

let num =[ 5 , 8 , 4 ]
num.push(7)

6-Como saber o tamanho de um array?Basta fazer da seguinte maneira:
num.length

Observação: Não precisa colocar parenteses no final de "length" pois 
não é um metodo e sim um atributo;

7-Para organizar os elementos dentro de um array vetor em ordem crescente,
basta usar o "sort()", por exemplo:
num.sort()

8-Para ver o conteúdo de algum elemento em determinado indíce, basta fazer
da seguinte maneira:
console.log(`${num[0]}`)

Aqui por exemplo estamos acessando o elemento no indíce 0 e mostrando o seu
conteúdo valor, que no caso é o primeiro elemento do array;

9-Para acessar o valor de todas as variaveis de um vetor sem que escrever
muito código, podemos usar os laços de repetições.Exemplo:

for(let pos=0; pos < valores.length; pos++){
    console.log(`A posição ${pos} tem o valor ${valores[pos]}`)
}

Então iremos mostrar na tela o conteúdo valor de todos os indíces do vetor,
de um em um,tendo como o teste lógico(true,false) o tamanho do vetor.Então
enquanto pos for menor que o comprimento do vetor, acrescente mais um e mostre me
o valor na posição [n].

10-Ainda temos outra opção de mostrar o valor das variáveis de uma maneira 
bem mais simples que o "FOR" tradicional.Podemos fazer da seguinte maneira:

for(let pos in valores){
    console.log(`A posição ${pos} tem o valor ${valores[pos]}`)
}

primeiro elemento dentro do for vai ser uma variavel, depois de "in" 
vira o nome do array.Parecido com o for each do Java.Então podemos ler da seguinte
maneira "para cada posição dentro do vetor num, mostre me a posição dentro de num";

11-Para buscar a posição(indíce) em que um determinado conteúdo valor está dentro
um vetor,podemos usar o "indexOf()" passando como parâmetro dentro 
dos parênteses o valor que você deseja buscar.Exemplo:

let pos = num.indexOf(7)

Irá buscar o valor sete dentro do vetor, caso ele encontre irá retornar 
o indice senão retorna "-1";

*** AULA 16 ***
Funções em JS

*Chamadas
*Parâmetros
*Retorno
*Ação

1-Funções são 'ações' executadas assim que são 'chamadas' ou em decorrência 
de algum 'evento';

2-Uma função pode receber'parâmetros' e retornar um 'resultado';

3-Exemplo de funções com parâmetros:

function soma(n1, n2){//função com dois parâmetros
    return n1+n2 //retorno da função utilizando os parâmetros
}

console.log(soma(2,5))//chamada para função passando os parâmentros 

Observação: caso esqueça de passar algum parâmetro, ele vai retornar um erro
undefined ou um "NaN"(Not a Number), para isso não ocorrer podemos fazer da 
seguinte forma:

function soma(n1=0, n2=0){
  return n1+n2 
}

Caso ele não passe parâmetros na chamada para função os valores por
default será zero;

4-Podemos passar um função dentro de uma variável.Exemplo:
//Passando uma função dentro de uma variavel
let v = function(x){
    return x*2
}

//Mostrando o valor de retorno da função pela variavel 'v'
console.log(v(5))

5-Podemos tamném usar a função de uma maneira recursiva,
fazer uma chamada pra função dentro dela mesmo.Por exemplo:
//Forma Recursiva
function fatorial(n){
    if(n==1){
        return 1
    }else{
        return n*fatorial(n-1)
    }
}
Entao criamos a função e fazemos a chamada dentro dela mesma;

***Aula 17 ***
Próximos Passos

1-Faça um curso de HTML e CSS;

2-Estude muito sobre funções;

3-Estude objetos em JS;

4-Estude sobre modularização;

5-Estude expressões regulares (RegEx);

6-Estude sobre JSON;

7-Estude sobre AJAX;

8-Estude sobre NodeJS;
