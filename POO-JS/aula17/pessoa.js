var Pessoa = function(_campos){

    if(_campos != undefined){  
        this.nome = _campos.nome
        this.telefone = _campos.telefone
        this.idade =  _campos.idade
        this.cpf= _campos.cpf   
    }
    else{//Para a classe filho herdar os atributos
        this.nome = ""
        this.telefone = ""
        this.idade =  ""
        this.cpf= ""   
    }

    this.Mostrar = function(){
       document.write(`Nome:  ${this.nome} <br>`)
       document.write(`Telefone: ${this.telefone}<br> `)
       document.write(`Idade: ${this.idade}<br>`)
       document.write(`Cpf: ${this.cpf}`)
    }

    this.GravarEmMemoria = function(){
        Pessoa.BaseDePessoas.push(this) 
    }
}
Pessoa.BaseDePessoas = [] 

Pessoa.todos = function(){//retornar todos os clientes
    var res = document.querySelector('div#resultado')

    res.innerHTML += "<h1>Estou executando um método de classe </h1><br> "

    for(var i=0; i < Pessoa.BaseDePessoas.length; i++){
        pessoa = Pessoa.BaseDePessoas[i]
       
        res.innerHTML +=`Nome:  ${pessoa.nome} <br>`
        res.innerHTML += `Telefone: ${pessoa.telefone}<br> `
        res.innerHTML += `Idade: ${pessoa.idade}<br>`
        res.innerHTML += "<hr>"
    }

}