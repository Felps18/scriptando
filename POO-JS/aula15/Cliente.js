var Cliente = function(_campos){//passagem de parâmetros no construtor
    /*
    Ajustes para mostrar na tela os atributos do objeto pelo hash
    undefined vem quando não for preenchido algum campo
    */
    if(_campos != undefined){
        if(_campos.nome !=undefined){
            this.nome = _campos.nome
        }else{
            this.nome=""
        }
        if(_campos.telefone !=undefined){
            this.telefone = _campos.telefone
        }else{
            this.telefone=""
        }
        if(_campos.idade != undefined){
            this.idade =  _campos.idade
        }else{
            this.idade=""
        }
        
    }

    this.Mostrar = function(){//Mostrar o objeto somente da instância(um unidade de objeto) 
       document.write(`Nome:  ${this.nome} <br>`)
       document.write(`Telefone: ${this.telefone}<br> `)
       document.write(`Idade: ${this.idade}<br>`)
    }

    this.GravarEmMemoria = function(){//ação para varias instâncias
        Cliente.clientes.push(this) //colocando objetos do tipo cliente dentro do array
    }
}
Cliente.clientes = [] //criando uma variável que serve para todos os meus clientes(uma instância da classe)
/*
debugger
depurar o código pelo f12
*/
Cliente.todos = function(){//retornar todos os clientes
    var res = document.querySelector('div#resultado')

    res.innerHTML += "<h1>Estou executando um método de classe </h1><br> "

    for(var i=0; i < Cliente.clientes.length; i++){
        cliente = Cliente.clientes[i]//pegando um cliente pelo indice e mostrando na tela
       
        res.innerHTML +=`Nome:  ${cliente.nome} <br>`
        res.innerHTML += `Telefone: ${cliente.telefone}<br> `
        res.innerHTML += `Idade: ${cliente.idade}<br>`
        res.innerHTML += "<hr>"
    }

}