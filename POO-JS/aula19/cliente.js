var Cliente = function(){
    this.CodigoDoCliente =""
    this.GravarEmMemoria = function(){//método sobreescrito da classe pessoa
        Cliente.Base.push(this)
        this.super.GravarEmMemoria()//chamada a um método superior gravado na classe pai
    }
    this.super = Pessoa.prototype
    this.Original = function(){
        console.log("Sobeescrevi o método Original")
        this.super.Original()
    }
}
Cliente.prototype =  new Pessoa()//herança da classe Pessoa
Cliente.Base = []