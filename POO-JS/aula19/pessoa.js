var Pessoa = function(_campos){

    if(_campos != undefined){  
        this.nome = _campos.nome
        this.telefone = _campos.telefone
        this.idade =  _campos.idade
        this.cpf= _campos.cpf   
    }
    else{//Para a classe filho herdar os atributos
        this.nome = ""
        this.telefone = ""
        this.idade =  ""
        this.cpf= ""   
    }

    this.Mostrar = function(){
       document.write(`Nome:  ${this.nome} <br>`)
       document.write(`Telefone: ${this.telefone}<br> `)
       document.write(`Idade: ${this.idade}<br>`)
       document.write(`Cpf: ${this.cpf}`)
    }
}
//herdando/implementando uma classe abstrata
Pessoa.prototype  =  Abstracao.prototype

Pessoa.prototype.Original = function(){
    console.log("função original")
}

//Sobreescrevendo o método da classe abstrata
Pessoa.prototype.GravarEmMemoria = function(){
    Pessoa.Base.push(this) 
}

Pessoa.Base = [] 

Pessoa.todos = function(){//retornar todos os clientes
    var res = document.querySelector('div#resultado')

    res.innerHTML += "<h1>Estou executando um método de classe </h1><br> "

    for(var i=0; i < Pessoa.Base.length; i++){
        pessoa = Pessoa.Base[i]
       
        res.innerHTML +=`Nome:  ${pessoa.nome} <br>`
        res.innerHTML += `Telefone: ${pessoa.telefone}<br> `
        res.innerHTML += `Idade: ${pessoa.idade}<br>`
        res.innerHTML += "<hr>"
    }

}