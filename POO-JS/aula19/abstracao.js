var Abstracao = function(){
    //Para que a classe se torne abstrata temos que  mexer no construtor
    if(this.constructor === Abstracao){
        throw new Error("Não pode instanciar a classe abstrata!")
    }

}
Abstracao.prototype.nome =""
Abstracao.prototype.telefone=""
Abstracao.prototype.idade=""
Abstracao.prototype.cpf=""
Abstracao.prototype.GravarEmMemoria = function(){
    //lançando uma exceção caso nao sobreesvreva o método GravarEmMemoria
    throw new Error ("Obrigatório a implementação do método GravarEmMemoria, para classe filhas")
}
