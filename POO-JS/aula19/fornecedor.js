var Fornecedor = function(){
    this.CNPJ=""

    this.super = Pessoa.prototype//sobreescrevendo os métodos da classe Pessoa

    this.entregarProdutos= function(){
        alert("o Fornecedor está entregando os produtos")
    }
    
    this.GravarEmMemoria = function(){//método sobreescrito da classe pessoa
        Fornecedor.Base.push(this)
        this.super.GravarEmMemoria()//chamada a um método superior gravado na classe pai
    }
}
Fornecedor.prototype =  new Pessoa()//herança da classe Pessoa
Fornecedor.Base = []